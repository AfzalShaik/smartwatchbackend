'use strict';

module.exports = function (Devicesinfo) {
    Devicesinfo.getDeviceId = function (regNo, cb) {
        if (regNo == '062909246651198') {
            Devicesinfo.findOne({'where' : {'regNo' : '062909246651198'}}, function(err, response) {
                cb(null, response);
            });
        } else {
            Devicesinfo.findOne({'where' : {'regNo' : regNo}}, function(err, response) {
                cb(null, response);
            });
        }
        
    }
    Devicesinfo.remoteMethod('getDeviceId', {
        description: 'To get Device ID',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'regNo',
            type: 'number',
            required: true,
            http: {
                source: 'query',
            },
        }],
        http: {
            path: '/getDeviceId',
            verb: 'get',
        },
    });
};
