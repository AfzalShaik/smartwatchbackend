'use strict';

module.exports = function (Watch) {
    Watch.getWatch = function (id, cb) {
        var net = require('net');
        var server = net.createServer(function (connection) {
            console.log('client connected');

            connection.on('end', function () {
                console.log('client disconnected');
            });
            // connection.write('Hello World!\r\n');
            //connection.pipe(connection);
            connection.on('data', function (data) {
                console.log('data::::', data);
                var textChunk = data.toString('utf8');
                console.log('text chunkkkk', textChunk);
                cb(null, textChunk);
            });
        });
        server.listen(8001, function () {
            console.log('server is listening');
        });
    }
    Watch.remoteMethod('getWatch', {
        description: 'client connection',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'id',
            type: 'number',

            http: {
                source: 'query',
            },
        }],
        http: {
            path: '/getWatch',
            verb: 'GET',
        },
    });
    // to get latest record

    Watch.getLatestRecord = function (deviceId, cb) {
        Watch.findOne({
            order: 'id DESC',
            "deviceId": deviceId,
            limit: 1
          }, function(err, response) {
              cb(null, response);
          });
    }
    Watch.remoteMethod('getLatestRecord', {
        description: 'client connection',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'deviceId',
            type: 'number',

            http: {
                source: 'query',
            },
        }],
        http: {
            path: '/getLatestRecord',
            verb: 'GET',
        },
    });
};
