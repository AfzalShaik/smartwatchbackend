'use strict';
var server = require('../../server/server');
module.exports = function (Sosmessage) {
    Sosmessage.observe('before save', function (ctx, next) {
        var listOfDevices = server.models.ListOfDevices;
        var customerDetails = server.models.CustomerDetails;
        var sms = server.models.sms;
        listOfDevices.findOne({
            order: 'id DESC',
            "deviceId": ctx.instance.deviceId,
            limit: 1
        }, function (listErr, listOut) {
            console.log("listOutlistOut", listOut);
            customerDetails.findOne({ 'where': { "aadharNum": listOut.aadharNum } }, function (custErr, custOut) {
                console.log('custoutttttttttt ', custOut);
                var smsObj = {};
                smsObj = {
                   "message" : ctx.instance.message,
                    "sos": [
                        custOut.emergency_contact_1,
                        custOut.emergency_contact_2
                    ]
                }
                
                sms.sendSOSSMS(smsObj, function (smsErr, smsOut) {
                    console.log('smssssssssssssss ', smsObj, smsOut);
                    next();
                });
            });
        });
    });
};
