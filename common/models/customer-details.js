'use strict';
var server = require('../../server/server');
module.exports = function (Customerdetails) {
    Customerdetails.postCustomerDetails = function (input, cb) {
        Customerdetails.create(input, function (err, response) {
            cb(null, response);
        });
    }
    Customerdetails.remoteMethod('postCustomerDetails', {
        description: 'Send Valid Data ',
        returns: {
            arg: 'data',
            type: 'object',
        },
        accepts: [{ arg: 'data', type: 'object', http: { source: 'body' } }],
        http: {
            path: '/postCustomerDetails',
            verb: 'POST',
        },
    });

    // to update customer data
    Customerdetails.updateCustomer = function (input, cb) {
        var listOfDevices = server.models.ListOfDevices;
        var sms = server.models.sms;
        Customerdetails.findOne({ 'where': { 'aadharNum': input.aadharNum } }, function (err, resp) {
            listOfDevices.findOne({ 'where': { 'aadharNum': input.aadharNum } }, function (err1, resp1) {
                var inputObj = {};
                inputObj = input;
                var smsObj = {};
                smsObj = {
                    "deviceSim": [
                        resp1.deviceSim,
                    ],
                    "emergencyContacts": [
                        resp.emergency_contact_1,
                        resp.emergency_contact_2
                    ]
                }
                resp.updateAttributes(inputObj, function (updateErr, updateOut) {
                    sms.sendSimSMS(smsObj, function (smsErr, smsOut) {
                        cb(null, updateOut);
                    });
                });
            });
        });
    }
    Customerdetails.remoteMethod('updateCustomer', {
        description: 'Send Valid Data ',
        returns: {
            arg: 'data',
            type: 'object',
        },
        accepts: [{ arg: 'data', type: 'object', http: { source: 'body' } }],
        http: {
            path: '/updateCustomer',
            verb: 'PUT',
        },
    });
    // To get Custoer Details
    Customerdetails.getCustomerDetails = function (aadharNum, cb) {
        Customerdetails.findOne({ 'where': { 'aadharNum': aadharNum.toString() } }, function (err, resp) {
            // console.log(err);
            var data = {};
            data.data = resp;
            cb(null, data);
        });
    }
    Customerdetails.remoteMethod('getCustomerDetails', {
        description: 'To get Device ID',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'aadharNum',
            type: 'number',
            required: true,
            http: {
                source: 'query',
            },
        }],
        http: {
            path: '/getCustomerDetails',
            verb: 'get',
        },
    });

};
