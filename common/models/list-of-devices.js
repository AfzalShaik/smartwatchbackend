'use strict';
var server = require('../../server/server');
module.exports = function (Listofdevices) {
    Listofdevices.observe('before save', function (ctx, next) {
        var sms = server.models.sms;
        var customer = server.models.customer;
        var customerDetails = server.models.CustomerDetails;
        // console.log('---------------------- ', ctx.instance);
        customerDetails.findOne({ 'where': { "aadharNum": ctx.instance.aadharNum } }, function (custErr, custOut) {
            // console.log(JSON.stringify(custOut));
            var input = {};
            input = {
                "deviceSim": [
                    ctx.instance.deviceSim,
                ],
                "emergencyContacts": [
                    custOut.emergency_contact_1,
                    custOut.emergency_contact_2
                ]
            }
            // console.log('inputttttttttttt ', input);
            sms.sendSimSMS(input, function(smsErr, smsOut) {
                console.log('smsssssssssssss ', smsOut);
                // cb(null, 'done')
                next();
        });
        });

        
    });
    Listofdevices.getListOfDevices = function (aadharNum, cb) {
        // console.log('userid-------------------- ', id);
        Listofdevices.find({ 'where': { "aadharNum": aadharNum } }, function (uesrErr, userOut) {
            cb(null, userOut);
        });
    }
    Listofdevices.remoteMethod('getListOfDevices', {
        description: 'client connection',
        returns: {
            type: 'object',
            root: true,
        },
        accepts: [{
            arg: 'aadharNum',
            type: 'string',

            http: {
                source: 'query',
            },
        }],
        http: {
            path: '/getListOfDevices',
            verb: 'GET',
        },
    });
};
