'use strict';
var server = require('../../server/server');
module.exports = function(Customer) {
Customer.validatesUniquenessOf('aadharNum', {message: 'Aadhar Number is already exists'});
Customer.validatesUniquenessOf('customerPhone', {message: 'Phone Number is  already exists'});
Customer.validate('aadharNum', function(err) {
    
    // check if name exists
    if (this.aadharNum) {
     if (this.aadharNum.toString().length !=12) {
        err('Aadhar Number is not Currect');
      }
     
    }
  });
Customer.validate('customerPhone', function(err) {
    
    // check if name exists
    if (this.customerPhone) {
     if (this.customerPhone.toString().length >=12) {
        err('Phone Number is not Currect');
      }
     
    }
  });

Customer.observe('after save', function(ctx, next) {
  var customerDetails = server.models.CustomerDetails;
  // console.log('ctx.instanceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ', ctx.instance);
    // customerDetails.findOne({'where' : {'id' : ctx.instance.id}}, function(custErr, custOut) {
      var obj = {};
      obj = ctx.instance;
      // obj.customerId = obj.id;
      delete obj.password;
      customerDetails.postCustomerDetails(obj, function(detailsErr, detailsOut) {
        // cb(null, detailsOut);
        next();
      });
    // });
});


  Customer.forgotPassword = function (email, callBack) {

    var callBck = callBack;

    Customer.findOne({"where": {"aadharNum": email}}, function (err, clientR) {
      if (err) {
        callBck(null, err);
      } else if (clientR == undefined) {

        var error = new Error("Your are not registered to our application");
        callBck(error, null);

      } else {
        var randomstring = require("randomstring");

        var otp = randomstring.generate({
          length: 6,
          charset: 'alphanumeric'
        });
var message= "Hi @CONTACTNAME Yours password is:"+otp;
        clientR.updateAttributes({"password": otp}, function (err, clientRNext) {
          if (err) {
            callBck(null, err);
          } else {
            var Sms = server.models.Sms;
             Sms.create({
        "customerId":clientR.id,
        "customerName":clientR.customerName,
         "aadharNum":clientR.aadharNum,
        "emergency_contact_1":clientR.customerPhone,
          "message":message
       
      }, function (errsd, res) {
if(!errsd){
  callBck(null,"SMS Sent Successfully");
}else{
   var error = new Error('Enter Valid details.');
              callBck(error); 
}

      });
           

          }


        });
      }

    });

  };
Customer.remoteMethod('forgotPassword', {
    description: "Enter Aadhar to reset the password",
    returns: {
      arg: 'client',
      type: 'array'
    },
    accepts: [{arg: 'aadharNum', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/forgotPassword',
      verb: 'get'
    }
  });

};
