'use strict';
var server = require('../../server/server');
module.exports = function (Sms) {
  Sms.observe('before save', function removeUnwantedField(ctx, next) {

    if (ctx.isNewInstance) {

      var date = new Date();

      var hour = date.getHours();
      hour = (hour < 10 ? "0" : "") + hour;

      var min = date.getMinutes();
      min = (min < 10 ? "0" : "") + min;



      var year = date.getFullYear();

      var month = date.getMonth() + 1;
      month = (month < 10 ? "0" : "") + month;

      var day = date.getDate();
      day = (day < 10 ? "0" : "") + day;


      ctx.instance.createdDate = day + "/" + month + "/" + year;
      ctx.instance.createdTime = hour + ":" + min;
      var message1;
      if (ctx.instance.location) {
        message1 = ctx.instance.message.toString().replace("@CONTACTNAME", ctx.instance.emergency_ct_name_1).replace("@CUSTNAME", ctx.instance.customerName).replace("@LOCATION", ctx.instance.location).replace(/ /g, "%20");

      } else {
        message1 = ctx.instance.message.toString().replace("@CONTACTNAME", ctx.instance.customerName).replace(/ /g, "%20");

      }
      console.log(" isNewInstance" + message1);
      var http = require('http');

      var options = {
        host: 'www.smslogin.mobi',
        path: '/spanelv2/api.php?username=caremiyitsol&password=c@remiyitsol&to=' + ctx.instance.emergency_contact_1.trim() + '&from=CAREMI&message=' + message1
      };


      var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {

        });
      });

      req.end();

      next();
    } else {
      next();
    }

  });


  Sms.sendsms = function (custDetls, callBac) {

    var Customer = server.models.Customer;
    if (custDetls.aadharNum) {
      Customer.find({ "where": { "aadharNum": custDetls.aadharNum } }, function (err, customers) {

        if (customers.length == 0) {
          var error = new Error('Your aadharNo is not registered.');
          callBac(error);
        }
        if (customers.length > 0) {

          var customer = customers[0];






          Sms.create({
            "customerId": customer.id,
            "customerName": customer.customerName,
            "location": custDetls.location,
            "aadharNum": customer.aadharNum,
            "emergency_contact_1": customer.emergency_contact_1,
            "emergency_ct_name_1": customer.emergency_ct_name_1,
            "message": custDetls.message
          }, function (errst, res) {

            Sms.create({
              "customerId": customer.id,
              "customerName": customer.customerName,
              "location": custDetls.location,
              "aadharNum": customer.aadharNum,
              "emergency_contact_1": customer.emergency_contact_2,
              "emergency_ct_name_1": customer.emergency_ct_name_2,
              "message": custDetls.message

            }, function (errsd, res) {
              if (!errst || !errsd) {
                callBac(null, "SMS Sent Successfully");
              } else {
                var error = new Error('Enter Valid details.');
                callBac(error, null);
              }

            });

          });



        } else {
          callBac();
        }
      });
    } else {
      var error = new Error('Enter Valid details.');
      callBac(error, null);
    }




  };
  Sms.remoteMethod('sendsms', {
    description: "Enter customer details",
    returns: {
      arg: 'result',
      type: 'object'
    },
    accepts: [{ arg: 'custDetls', type: 'object', required: true, http: { source: 'body' } }],
    http: {
      path: '/sendsms',
      verb: 'POST'
    }
  });


  Sms.shakingsos = function (custDetls, callBac) {

    var Customer = server.models.Customer;
    var sms = server.models.Sms;
    if (custDetls.aadharNum) {
      Customer.find({ "where": { "aadharNum": custDetls.aadharNum } }, function (err, customers) {

        if (customers.length == 0) {
          var error = new Error('Your aadharNo is not registered.');
          callBac(error, null);
        }
        if (customers.length > 0) {

          var date = new Date();

          var hour = date.getHours();
          hour = (hour < 10 ? "0" : "") + hour;

          var min = date.getMinutes();
          min = (min < 10 ? "0" : "") + min;
          var year = date.getFullYear();

          var month = date.getMonth() + 1;
          month = (month < 10 ? "0" : "") + month;

          var day = date.getDate();
          day = (day < 10 ? "0" : "") + day;

          var datevalue = day + "/" + month + "/" + year;
          var prasenttime = hour + ":" + min;

          var newdate = new Date();
          newdate.setTime(newdate.getTime() - 30 * 60000);
          var hournewdate = newdate.getHours();
          hournewdate = (hournewdate < 10 ? "0" : "") + hournewdate;
          var minnewdate = newdate.getMinutes();
          minnewdate = (minnewdate < 10 ? "0" : "") + minnewdate;
          var pasttime = hournewdate + ":" + minnewdate;
          var customer = customers[0];
          sms.find({ "where": { "aadharNum": custDetls.aadharNum, "createdDate": datevalue, "createdTime": { "between": [pasttime, prasenttime] } } }, function (err, customerssms) {
            if (customerssms.length == 0) {

              Sms.create({
                "customerId": customer.id,
                "customerName": customer.customerName,
                "location": custDetls.location,
                "aadharNum": customer.aadharNum,
                "emergency_contact_1": customer.emergency_contact_1,
                "emergency_ct_name_1": customer.emergency_ct_name_1,
                "message": custDetls.message
              }, function (errst, res) {

                Sms.create({
                  "customerId": customer.id,
                  "customerName": customer.customerName,
                  "location": custDetls.location,
                  "aadharNum": customer.aadharNum,
                  "emergency_contact_1": customer.emergency_contact_2,
                  "emergency_ct_name_1": customer.emergency_ct_name_2,
                  "message": custDetls.message

                }, function (errsd, res) {
                  if (!errst || !errsd) {
                    callBac(null, "SMS Sent Successfully");
                  } else {
                    var error = new Error('Enter Valid details.');
                    callBac(error, null);
                  }

                });

              });

            }

            if (customerssms.length != 0) {
              var error = new Error('SMS Sent Already.');
              callBac(error, null);
            }
          });



        } else {
          callBac();
        }
      });
    } else {
      var error = new Error('Enter Valid details.');
      callBac(error, null);
    }




  };

  Sms.remoteMethod('shakingsos', {
    description: "Enter customer details",
    returns: {
      arg: 'result',
      type: 'object'
    },
    accepts: [{ arg: 'custDetls', type: 'object', required: true, http: { source: 'body' } }],
    http: {
      path: '/shakingsos',
      verb: 'POST'
    }
  });
  // send sos to device sim
  Sms.sendSimSMS = function (input, cb) {
    var utf8 = require('utf8');
    // var message = input.message.toString().replace(/ /g, "%20");

    var http = require('http');
    // var options = {
    //   host: 'www.smslogin.mobi',
    //   path: '/spanelv2/api.php?username=telepathy&password=telepathy&to='+ctx.instance.destination+'&from=TLPATY&message='+ctx.instance.message
    // };

    var des = input.deviceSim.join();
    // console.log("destination"+des);
    for (var i = 0; i < input.emergencyContacts.length; i++) {
      if (i == 0) {
        var message = 'pw,123456,sos1,' + input.emergencyContacts[i] + '#'
        // message = message.toString().replace(/ /g, "%20");
      }
      if (i == 1) {
        var message = 'pw,123456,sos2,' + input.emergencyContacts[i] + '#'
        // message = message.toString().replace(/ /g, "%20");
      }
      var options = {
        host: 'smslogin.mobi',
        path: '/spanelv2/api.php?username=smartcity&password=smartcity&from=CAREME&to=' + des + '&message=' + message
      };

      var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
        });
      });
      req.end();
    }

    //your logic goes here
    // next();
    cb(null, 'message sent')
  }
  Sms.remoteMethod('sendSimSMS', {
    description: "Enter customer details",
    returns: {
      arg: 'result',
      type: 'object'
    },
    accepts: [{ arg: 'input', type: 'object', required: true, http: { source: 'body' } }],
    http: {
      path: '/sendSimSMS',
      verb: 'POST'
    }
  });
  // send message to sos numbers
  Sms.sendSOSSMS = function (input, callBC) {
    var utf8 = require('utf8');
    // var message = input.message.toString().replace(/ /g, "%20");

    var http = require('http');
    // var options = {
    //   host: 'www.smslogin.mobi',
    //   path: '/spanelv2/api.php?username=telepathy&password=telepathy&to='+ctx.instance.destination+'&from=TLPATY&message='+ctx.instance.message
    // };

    var des = input.sos.join();
    
    for (var i = 0; i < input.sos.length; i++) {
      console.log("destination123 : "+ des);
      var destin = [];
      destin = [
        input.sos[i]
      ]
      var pathVar = '/spanelv2/api.php?username=smartcity&password=smartcity&from=CAREME&to=' + des + '&message=' + input.message;
      var options = {
        host: 'smslogin.mobi',
        path: encodeURI(pathVar)
      };

      var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
        });
      });
      req.end();
    }

    //your logic goes here
    // next();
    callBC(null, 'message sent')
  }
  Sms.remoteMethod('sendSOSSMS', {
    description: "Enter customer details",
    returns: {
      arg: 'result',
      type: 'object'
    },
    accepts: [{ arg: 'input', type: 'object', required: true, http: { source: 'body' } }],
    http: {
      path: '/sendSOSSMS',
      verb: 'POST'
    }
  });
};


